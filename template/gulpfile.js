// Подключаем Gulp
const gulp = require('gulp')

// Подключаем плагины Gulp
const gulpIf = require('gulp-if')
const concat = require('gulp-concat')
const plumber = require('gulp-plumber')
const browserSync = require('browser-sync').create()
const rimraf = require('rimraf')
const sass = require('gulp-sass')
const cssnano = require('gulp-cssnano')
const cleanCss = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')
const imagemin = require('gulp-imagemin')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const sourcemaps = require('gulp-sourcemaps')

// Конфигурация index
const page = 'index'
const config = {
	src: {
		html: `./src/${page}.html`,
		scss: './src/scss/**/*.scss',
		js: './src/js/**/*.js',
		img: './src/images/',
		fonts: './src/fonts/**/'
	},
	dist: {
		htmlPath: './dist',
		htmlName: `${page}.html`,
		cssPath: './dist/css',
		cssName: `style.${page}.min.css`,
		jsPath: './dist/js',
		jsName: `script.${page}.min.js`,
		imgPath: './dist/images',
		fontsPath: './dist/fonts'
	},
	isDevelop: false
}

// Очистка папки dist при сборке продакшена
gulp.task('clean', cb => {
	rimraf('./dist', cb)
})

// Копирование файлов HTML в папку dist
gulp.task('html', () =>
	gulp
		.src(config.src.html)
		.pipe(concat(config.dist.htmlName))
		.pipe(gulp.dest(config.dist.htmlPath))
		.pipe(browserSync.stream())
)

// Объединение, компиляция Sass в CSS,
// простановка венд. префиксов и дальнейшая минимизация кода
gulp.task('scss', () =>
	gulp
		.src(config.src.scss)
		.pipe(plumber({ errorHandler: true }))
		.pipe(gulpIf(config.isDevelop, sourcemaps.init()))
		.pipe(
			sass({
				includePaths: require('node-normalize-scss').includePaths
			})
		)
		.pipe(concat(config.dist.cssName))
		.pipe(plumber.stop())
		.pipe(
			autoprefixer({
				browsers: ['last 2 versions'],
				cascade: false
			})
		)
		// .pipe(gulpIf(!config.isDevelop, cleanCss()))
		.pipe(gulpIf(!config.isDevelop, cssnano()))
		.pipe(gulpIf(config.isDevelop, sourcemaps.write()))
		.pipe(gulp.dest(config.dist.cssPath))
		.pipe(browserSync.stream())
)

// Объединение и сжатие JS-файлов
gulp.task('scripts', () =>
	gulp
		.src(config.src.js)
		.pipe(plumber({ errorHandler: true }))
		.pipe(gulpIf(config.isDevelop, sourcemaps.init()))
		.pipe(concat(config.dist.jsName))
		.pipe(plumber.stop())
		.pipe(
			babel({
				presets: ['env']
			})
		)
		.pipe(gulpIf(!config.isDevelop, uglify()))
		.pipe(gulpIf(config.isDevelop, sourcemaps.write('.')))
		.pipe(gulp.dest(config.dist.jsPath))
)

// Сжимаем картинки
gulp.task('imgs', () =>
	gulp
		.src(config.src.img + '*.+(jpg|jpeg|png|gif|svg)')
		.pipe(
			imagemin([
				imagemin.gifsicle({ interlaced: true }),
				imagemin.jpegtran({ progressive: true }),
				imagemin.optipng({ optimizationLevel: 5 }),
				imagemin.svgo({
					plugins: [{ removeViewBox: true }, { cleanupIDs: false }]
				})
			])
		)
		.pipe(gulp.dest(config.dist.imgPath))
)

// Генерация шрифтов
gulp.task('fonts', () =>
	gulp
		.src(config.src.fonts + '*.+(otf|eot|woff2|woff|ttf|svg)')
		.pipe(gulp.dest(config.dist.fontsPath))
)

// Задача слежения за измененными файлами
gulp.task('watch', () => {
	browserSync.init({
		server: {
			baseDir: './dist',
			browser: `C:\\Program Files\\Firefox Developer Edition\\firefox.exe`
		}
	})

	gulp.watch(config.src.html, ['html']).on('change', browserSync.reload)
	gulp.watch(config.src.scss, ['scss']).on('change', browserSync.reload)
	gulp.watch(config.src.js, ['scripts']).on('change', browserSync.reload)
	// gulp.watch(config.src.img + '*.+(jpg|jpeg|png|gif)', ['imgs'])
})

///// Таски ///////////////////////////////////////

// Запуск тасков по умолчанию
gulp.task('default', ['html', 'scss', 'scripts', 'watch'])
gulp.task('prepare', ['imgs', 'fonts'])
gulp.task('clean', ['clean'])
gulp.task('full', ['html', 'scss', 'scripts', 'imgs', 'fonts'])
