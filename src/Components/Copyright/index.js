import React from 'react'
import './style.css'

const Copyright = () => {
	return (
		<div className="copy">
			<span>
				Разработка сайта - <a href="http:\\ostkost.ru">ostkost.ru</a> |{' '}
				{new Date().getFullYear()}
				&nbsp;&copy; Все&nbsp;права&nbsp;защищены
			</span>
			<hr />
			<span>
				Сайт носит информационный характер,и не является публичной
				офертой
			</span>
		</div>
	)
}

export default Copyright
