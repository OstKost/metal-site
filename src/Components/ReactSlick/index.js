import React from 'react'
import Slider from 'react-slick'
import './style.css'

import image1 from '../../images/400x200/project (1).jpg'
import image2 from '../../images/400x200/project (2).jpg'
import image3 from '../../images/400x200/project (3).jpg'
import image4 from '../../images/400x200/project (4).jpg'
import image5 from '../../images/400x200/project (5).jpg'
import image6 from '../../images/400x200/project (6).jpg'
import image7 from '../../images/400x200/project (7).jpg'
import image8 from '../../images/400x200/project (8).jpg'
import image9 from '../../images/400x200/project (9).jpg'
import image10 from '../../images/400x200/project (10).jpg'
import image11 from '../../images/400x200/project (11).jpg'
import image12 from '../../images/400x200/project (12).jpg'
import image13 from '../../images/400x200/project (13).jpg'
import image14 from '../../images/400x200/project (14).jpg'
import image15 from '../../images/400x200/project (15).jpg'

export default class ReactSlick extends React.Component {
	render() {
		const settings = {
			dots: true,
			infinite: true,
			speed: 600,
			slidesToShow: 5,
			slidesToScroll: 3,
			slide: 'image',
			autoplay: true,
      autoplaySpeed: 4000,
      pauseOnHover: true,
      swipeToSlide: true,
      lazyLoad: true,
			responsive: [
				{
					breakpoint: 1750,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4
					}
				},
				{
					breakpoint: 1400,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 1070,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 750,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: false
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: false,
						arrows: false
					}
				}
			]
		}

		return (
			<div className="slickContainer">
				<Slider {...settings}>
					<div>
						<img src={image1} alt="Фото проекта" />
					</div>
					<div>
						<img src={image2} alt="Фото проекта" />
					</div>
					<div>
						<img src={image3} alt="Фото проекта" />
					</div>
					<div>
						<img src={image4} alt="Фото проекта" />
					</div>
					<div>
						<img src={image5} alt="Фото проекта" />
					</div>
					<div>
						<img src={image6} alt="Фото проекта" />
					</div>
					<div>
						<img src={image7} alt="Фото проекта" />
					</div>
					<div>
						<img src={image8} alt="Фото проекта" />
					</div>
					<div>
						<img src={image9} alt="Фото проекта" />
					</div>
					<div>
						<img src={image10} alt="Фото проекта" />
					</div>
					<div>
						<img src={image11} alt="Фото проекта" />
					</div>
					<div>
						<img src={image12} alt="Фото проекта" />
					</div>
					<div>
						<img src={image13} alt="Фото проекта" />
					</div>
					<div>
						<img src={image14} alt="Фото проекта" />
					</div>
					<div>
						<img src={image15} alt="Фото проекта" />
					</div>
				</Slider>
			</div>
		)
	}
}
