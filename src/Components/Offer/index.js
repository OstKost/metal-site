import React, { Component } from 'react'
import './style.css'
import MyModal from '../MyModal'

export default class Offer extends Component {
	state = {
		modalOpened: false
	}

	openModal = () => () => {
		this.setState({ modalOpened: true })
	}

	closeModal = () => {
		this.setState({ modalOpened: false })
	}

	render() {
		return (
			<section className="offer" id="offer">
				<h3 className="offer__header">Специальное предложение</h3>
				<ul className=" offer__list">
					<li className="offer__item">Выезд на место</li>
					<li className="offer__item">
						Изготовление на нашей производственной площадке
					</li>
					<li className="offer__item">
						Доставка конструкции на место
					</li>
					<li className="offer__item">
						Сбор металлоконструкции "под ключ"
					</li>
				</ul>
				<button
					type="button"
					className="offer__btn"
					onClick={this.openModal()}
				>
					Оставить заявку
				</button>
				<MyModal
					closeModal={this.closeModal}
					opened={this.state.modalOpened}
				/>
			</section>
		)
	}
}
