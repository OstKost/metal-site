import React, { Component } from 'react'
import './style.css'

import ReactSlick from '../ReactSlick'

export default class Projects extends Component {
	render() {
		return (
			<div>
				<section className="section projects" id="projects">
					<div className="container">
						<div className="sectionHeader">
							<h2 className="sectionHeader__text">
								Наши&nbsp;проекты
							</h2>
							<div className="sectionHeader__underline" />
						</div>
					</div>
				</section>
				<ReactSlick />
			</div>
		)
	}
}
