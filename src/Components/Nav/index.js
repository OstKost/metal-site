import React, { Component } from 'react'
import './style.css'
import Menu from '@material-ui/icons/Menu'
import { Drawer } from '@material-ui/core/'
import MyModal from '../MyModal'

export default class Nav extends Component {
	state = {
		drawerOpened: false,
		modalOpened: false
	}

	toggleDrawer = open => () => {
		this.setState({ drawerOpened: open })
	}

	openModal = () => () => {
		this.setState({ modalOpened: true })
	}

	closeModal = () => {
		this.setState({ modalOpened: false })
	}

	render() {
		return (
			<nav className="topNav">
				<div className="topNav__logo">НАВЕСЫ РОСТОВ</div>
				<div
					className="topNav__menuBtn mobile"
					onClick={this.toggleDrawer(true)}
				>
					<Menu />
					&nbsp;МЕНЮ
				</div>
				<div className="topNav__links">
					<div className="topNav__block nomobile">
						<a href="#about" className="topNav__link">
							О&nbsp;нас
						</a>
						<a href="#projects" className="topNav__link">
							Наши&nbsp;проекты
						</a>
					</div>
					<div className="topNav__block nomobile">
						<a href="#benefits" className="topNav__link">
							Преимущества
						</a>
						<a href="#reviews" className="topNav__link">
							Отзывы
						</a>
					</div>
					<div className="topNav__block">
						<div
							className="topNav__link topNav__link-button"
							onClick={this.openModal()}
						>
							Заказать&nbsp;звонок
						</div>
					</div>
				</div>

				<Drawer
					anchor="top"
					open={this.state.drawerOpened}
					onClose={this.toggleDrawer(false)}
				>
					<div
						tabIndex={0}
						role="button"
						onClick={this.toggleDrawer(false)}
						onKeyDown={this.toggleDrawer(false)}
					>
						<p>
							<a href="#about" className="mobile__link">
								О&nbsp;нас
							</a>
						</p>
						<p>
							<a href="#projects" className="mobile__link">
								Наши&nbsp;проекты
							</a>
						</p>
						<p>
							<a href="#benefits" className="mobile__link">
								Преимущества
							</a>
						</p>
						<p>
							<a href="#reviews" className="mobile__link">
								Отзывы
							</a>
						</p>
						<p>
							<a href="#contacts" className="mobile__link">
								Контакты
							</a>
						</p>
					</div>
				</Drawer>

				<MyModal
					closeModal={this.closeModal}
					opened={this.state.modalOpened}
				/>
			</nav>
		)
	}
}
