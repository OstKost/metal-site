import React, { Component } from 'react'
import './style.css'
import { PhoneIphone, Phone, Email } from '@material-ui/icons/'

import MyModal from '../MyModal'

export default class Footer extends Component {
	state = {
		modalOpened: false,
		formData: {
			name: '',
			contacts: '',
			comment: ''
		}
	}

	openModal = () => () => {
		this.setState({ modalOpened: true })
	}

	closeModal = () => {
		this.setState({ modalOpened: false })
	}

	updateTextForm = name => event => {
		const newFormData = { ...this.state.formData }
		newFormData[[name]] = event.target.value
		this.setState({
			formData: newFormData
		})
	}

	render() {
		return (
			<footer className="section contacts" id="contacts">
				<div className="container">
					<div className="contacts__block">
						<div className="sectionHeader">
							<h2 className="sectionHeader__text">
								Связаться с&nbsp;нами
							</h2>
							<div className="sectionHeader__underline" />
						</div>
						<ul className="contacts__list">
							<li className="contacts__listItem">
								<a
									href="callto:+7-903-401-8665"
									className="contacts__link"
								>
									<PhoneIphone className="contacts__icon"></PhoneIphone>
									<span className="contacts__listText">
										+7-903-401-8665
									</span>
								</a>
							</li>
							<li className="contacts__listItem">
								<a
									href="callto:221-86-65"
									className="contacts__link"
								>
									<Phone className="contacts__icon"></Phone>
									<span className="contacts__listText">
										221-86-65
									</span>
								</a>
							</li>
							<li className="contacts__listItem">
								<a
									href="mailto:vvgrig78@gmail.com"
									className="contacts__link"
								>
									<Email className="contacts__icon"></Email>
									<span className="contacts__listText">
										vvgrig78@gmail.com
									</span>
								</a>
							</li>
						</ul>
					</div>
					<div className="contacts__block">
						<div className="sectionHeader">
							<h2 className="sectionHeader__text">
								Написать&nbsp;нам
							</h2>
							<div className="sectionHeader__underline" />
						</div>

						<div className="contacts__formWr">
							<form
								id="footerForm"
								className="contacts__form btForm"
								method="post"
								action=""
							>
								<div className="btForm__fRow">
									<div className="btForm__col">
										<label
											className="btForm__label"
											htmlFor="btName"
										>
											Ваше&nbsp;имя
										</label>
										<input
											id="btName"
											name="name"
											type="text"
											className="btForm__inp"
											onChange={this.updateTextForm(
												'name'
											)}
										/>
									</div>
									<div className="btForm__col">
										<label
											className="btForm__label"
											htmlFor="btData"
										>
											Телефон
										</label>
										<input
											id="btData"
											name="data"
											type="phone"
											className="btForm__inp"
											onChange={this.updateTextForm(
												'contacts'
											)}
										/>
									</div>
								</div>
								<div className="btForm__sRow">
									<label
										className="btForm__labelTa"
										htmlFor="btText"
									>
										Сообщение
									</label>
									<textarea
										id="btText"
										name="message"
										className="btForm__inpTA"
										onChange={this.updateTextForm(
											'comment'
										)}
									/>
								</div>
								<div
									className="btForm__btn"
									onClick={this.openModal()}
								>
									Оставить заявку
								</div>
							</form>
						</div>
					</div>
				</div>
				<MyModal
					closeModal={this.closeModal}
					opened={this.state.modalOpened}
					title="Подтвердите отправку"
					name={this.state.formData.name}
					contacts={this.state.formData.contacts}
					comment={this.state.formData.comment}
				/>
			</footer>
		)
	}
}
