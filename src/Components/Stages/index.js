import React, { Component } from 'react'
import './style.css'

const stages = [
	{
		num: '01',
		name: 'Встреча',
		text:
			'Вы оставляете заявку удобным для Вас способом (на сайте или по телефону). Мы связываемся с Вами и договариваемся о встрече. Наши специалисты приедут в удобное для Вас время для осмотра объекта и обсуждения плана работ.'
	},
	{
		num: '02',
		name: 'Согласование',
		text:
			'Мы обсуждаем с заказчиком все особенности конструкции, учитывая множество факторов. Составляем смету и проект, который будет реализован на объекте.'
	},
	{
		num: '03',
		name: 'Договор',
		text:
			'После согласования наш специалист выезжает в удобное Вам время, для заключения договора.'
	},
	{
		num: '04',
		name: 'Изготовление',
		text:
			'После подписания договора начинается процесс изготовления и дальнейшие работы на объекте.'
	}
]

export default class Stages extends Component {
	state = {
		activeStage: 0
	}

	handleClick = value => () => {
		this.setState({
			activeStage: value
		})
	}

	render() {
		return (
			<section className="section stages" id="stages">
				<div className="container">
					<div className="sectionHeader">
						<h2 className="sectionHeader__text">
							Как&nbsp;мы&nbsp;работаем
						</h2>
						<div className="sectionHeader__underline" />
					</div>

					<div className="stageRow">
						<div className="stageLine">
							{stages.map((item, index) => {
								return (
									<div
										className={`stageLine__item ${
											this.state.activeStage === index
												? 'stageLine__item-active'
												: ''
										}`}
										key={index}
										onClick={this.handleClick(index)}
									>
										<div className="stageLine__num">
											{item.num}
										</div>
										<div className="stageLine__name">
											{item.name}{' '}
										</div>
										<div className="stageLine__vline" />
									</div>
								)
							})}
						</div>

						<div className="stageDesc">
							<p className="stageDesc__name">
								{stages[this.state.activeStage].num +
									' ' +
									stages[this.state.activeStage].name}
							</p>
							<p className="stageDesc__text">
								{stages[this.state.activeStage].text}
							</p>
						</div>
					</div>
				</div>
			</section>
		)
	}
}
