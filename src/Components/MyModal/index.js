import React, { Component } from 'react'
import './style.css'
import {
	Modal,
	TextField,
	Button,
	FormGroup,
	FormControlLabel,
	Checkbox
} from '@material-ui/core/'
import Close from '@material-ui/icons/Close'
import axios from 'axios'

export default class MyModal extends Component {
	state = {
		formData: {
			name: this.props.name || '',
			contacts: this.props.contacts || '',
			comment: this.props.comment || '',
			approval: true
		},
		formValid: false,
		formValidation: {
			name: {
				valid: true,
				touched: this.props.name || false,
				helperText: ' '
			},
			contacts: {
				valid: true,
				touched: this.props.contacts || false,
				helperText: ' '
			},
			approval: {
				checked: true
			}
		}
	}

	componentWillReceiveProps() {
		const newFormData = { ...this.state.formData }
		const newFormValidation = { ...this.state.formValidation }

		if (this.props.name) {
			newFormData.name = this.props.name
			newFormValidation.name.touched = true
		}

		if (this.props.contacts) {
			newFormData.contacts = this.props.contacts
			newFormValidation.contacts.touched = true
		}

		if (this.props.comment) {
			newFormData.comment = this.props.comment
		}

		this.setState({
			formData: newFormData,
			formValidation: newFormValidation
		})

		setTimeout(() => {
			this.validateForm()
		}, 50)
	}

	componentDidMount() {
		this.validateForm()
	}

	updateTextForm = name => event => {
		const newFormData = { ...this.state.formData }
		newFormData[[name]] = event.target.value
		this.setState({
			formData: newFormData
		})
		setTimeout(() => {
			this.validateForm()
		}, 50)
	}

	updateChekedForm = () => event => {
		const newFormData = { ...this.state.formData }
		newFormData.approval = event.target.checked
		this.setState({
			formData: newFormData
		})
		setTimeout(() => {
			this.validateForm()
		}, 50)
	}

	validateForm = () => {
		const newFormValidation = { ...this.state.formValidation }
		// name
		if (
			this.state.formData.name.length < 3 &&
			this.state.formValidation.name.touched
		) {
			newFormValidation.name.valid = false
			newFormValidation.name.helperText = 'Должно быть минимум 3 буквы'
		} else {
			newFormValidation.name.valid = true
			newFormValidation.name.helperText = ' '
		}

		if (
			!this.state.formData.name &&
			this.state.formValidation.name.touched
		) {
			newFormValidation.name.valid = false
			newFormValidation.name.helperText = 'Имя не должно быть пустым'
		} else {
			newFormValidation.name.valid = true
			newFormValidation.name.helperText = ' '
		}

		// contacts
		if (
			this.state.formData.contacts.length < 5 &&
			this.state.formValidation.contacts.touched
		) {
			newFormValidation.contacts.valid = false
			newFormValidation.contacts.helperText =
				'Должно быть минимум 5 символов'
		} else {
			newFormValidation.contacts.valid = true
			newFormValidation.contacts.helperText = ' '
		}

		if (
			!this.state.formData.contacts &&
			this.state.formValidation.contacts.touched
		) {
			newFormValidation.contacts.valid = false
			newFormValidation.contacts.helperText = 'Введите почту или телефон'
		} else {
			newFormValidation.contacts.valid = true
			newFormValidation.contacts.helperText = ' '
		}

		// approval
		if (!this.state.formData.approval) {
			newFormValidation.approval.checked = false
		} else {
			newFormValidation.approval.checked = true
		}

		this.setState({
			formValidation: newFormValidation,
			formValid:
				this.state.formValidation.name.touched &&
				this.state.formValidation.contacts.touched &&
				newFormValidation.approval.checked &&
				newFormValidation.name.valid &&
				newFormValidation.contacts.valid
		})
	}

	updateTouchForm = name => () => {
		const newFormValidation = { ...this.state.formValidation }
		newFormValidation[[name]].touched = true
		this.setState({
			formValidation: newFormValidation
		})
		setTimeout(() => {
			this.validateForm()
		}, 50)
	}

	sendMail = () => {
		// Send a POST request
		axios({
			method: 'post',
			url: '../../scripts/phpsendmail.php',
			data: {
				name: this.state.formData.name,
				contacts: this.state.formData.contacts,
				comment: this.state.formData.comment
			}
		})
			.then(response => {
				if (response.data === 'Письмо отправлено') {
					alert(response.data)
					this.props.closeModal()
				}

				if (response.data === 'Ошибка при отправке почты') {
					alert('Произошла ошибка при отправлке.\nПопробуйте позже.')
				}
			})
			.catch(error => {
				alert('Произошла ошибка при отправлке.\nПопробуйте позже.')
				console.log('error', error)
			})
		// .then(function() {
		// 	console.log('always executed')
		// })
	}

	render() {
		return (
			<Modal
				open={this.props.opened}
				// onClose={this.props.closeModal}
			>
				<div className="modal">
					<div className="modal__header">
						<h3>{this.props.title || 'Заполните форму'}</h3>
						<Close className='modal__closeIcon' onClick={this.props.closeModal}/>						
					</div>
					<FormGroup>
						<TextField
							id="modal-name"
							label="Ваше имя"
							value={this.state.formData.name}
							className="modal__input modal__input-name"
							helperText={
								this.state.formValidation.name.helperText
							}
							margin="dense"
							onChange={this.updateTextForm('name')}
							onBlur={this.updateTouchForm('name')}
							error={!this.state.formValidation.name.valid}
							style={
								!this.state.formValidation.approval.checked
									? {
											overflow: 'hidden',
											height: '0px',
											margin: '0px'
									  }
									: {}
							}
						/>
						<TextField
							id="modal-contacts"
							label="Телефон или почта"
							value={this.state.formData.contacts}
							className="modal__input modal__input-contacts"
							helperText={
								this.state.formValidation.contacts.helperText
							}
							margin="dense"
							onChange={this.updateTextForm('contacts')}
							onBlur={this.updateTouchForm('contacts')}
							error={!this.state.formValidation.contacts.valid}
							style={
								!this.state.formValidation.approval.checked
									? {
											overflow: 'hidden',
											height: '0px',
											margin: '0px'
									  }
									: {}
							}
						/>
						<TextField
							id="modal-comment"
							label="Комментарий"
							multiline
							rowsMax="4"
							value={this.state.formData.comment}
							onChange={this.updateTextForm('comment')}
							className="modal__input modal__input-comment"
							margin="dense"
							style={
								!this.state.formValidation.approval.checked
									? {
											overflow: 'hidden',
											height: '0px',
											margin: '0px'
									  }
									: {}
							}
						/>
						<FormControlLabel
							style={{ marginTop: '15px' }}
							control={
								<Checkbox
									checked={this.state.formData.approval}
									onChange={this.updateChekedForm()}
									value="approval"
									color="primary"
								/>
							}
							label="Я подтверждаю своё согласие на обработку персональных данных по ФЗ-152"
						/>
						<div>
							<Button
								variant="contained"
								color="primary"
								className="modal__btn"
								disabled={!this.state.formValid}
								style={{ marginTop: '20px' }}
								onClick={this.sendMail}
							>
								Отправить
							</Button>
							<div
								className="modal__btnHelper"
								style={{
									height: this.state.formValidation.approval
										.checked
										? '0px'
										: '102px'
								}}
							>
								<p>
									По закону, для отправки данных Вы обязаны
									подтвердить своё согласие на их обработку.
								</p>
								<p>
									Но Вы также можете позвонить нам напрямую.
								</p>
							</div>
						</div>
					</FormGroup>
				</div>
			</Modal>
		)
	}
}
