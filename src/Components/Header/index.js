import React, { Component } from 'react'
import './style.css'
import { PhoneIphone, Phone, CheckCircleOutline } from '@material-ui/icons/'

import Nav from '../Nav'

export default class Header extends Component {
	render() {
		return (
			<section className="section header" id="header">
				<div className="container">
					<Nav />

					<div className="mainHeader">
						<h1 className="mainHeader__text">
							Конструкции из&nbsp;металла
							<br />
							"под&nbsp;ключ"
						</h1>
						<div className="mainHeader__underline" />
						<a
							href="callto:+7-903-401-8665"
							className="mainHeader__phone"
						>
							<PhoneIphone className="mainHeader__icon" />
							&nbsp;+7-903-401-8665
						</a>
						<a
							href="callto:221-86-65"
							className="mainHeader__phone"
						>
							<Phone className="mainHeader__icon" />
							&nbsp;221-86-65
						</a>
					</div>

					<div className="mainList">
						<div className="mainList__row">
							<CheckCircleOutline className="mainList__icon" />
							<div className="mainList__item">
								Навесы,&nbsp;заборы
							</div>
						</div>
						<div className="mainList__row">
							<CheckCircleOutline className="mainList__icon" />
							<div className="mainList__item">
								Ангары,&nbsp;площадки
							</div>
						</div>
						<div className="mainList__row">
							<CheckCircleOutline className="mainList__icon" />
							<div className="mainList__item">
								Сложные металлоконструкции
							</div>
						</div>
						<div className="mainList__row">
							<CheckCircleOutline className="mainList__icon" />
							<div className="mainList__item">
								Своя производственная&nbsp;база
							</div>
						</div>
					</div>
				</div>
			</section>
		)
	}
}
