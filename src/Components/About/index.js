import React, { Component } from 'react'
import './style.css'

export default class About extends Component {
	render() {
		return (
			<section className="section about" id='about'>
				<div className="container">
					<div className="sectionHeader">
						<h2 className="sectionHeader__text">О&nbsp;нас</h2>
						<div className="sectionHeader__underline" />
					</div>
					<div className="aboutRow">
						<div className="aboutRow__col">
							<p className="aboutRow__text">
								Изготовление навесов из поликарбоната и
								профнастила, в том числе с элементами
								художественной ковки.
							</p>
							<p className="aboutRow__text">
								Изготовление металлических ворот, с учётом
								пожеланий заказчика, строительство заборов,
								лестниц, павильонов.
							</p>
							<p className="aboutRow__text">
								Изготовление и монтаж производственных
								помещений, ангаров, в том числе быстровозводимых
								зданий, а также промышленных стальных
								конструкций.
							</p>
						</div>
						<div className="aboutRow__col">
							<h3 className="aboutRow__bigText">
								Работаем&nbsp;с{' '}
								<strong className="aboutRow__selectText">
									2010&nbsp;года
								</strong>
							</h3>
							<h3 className="aboutRow__bigText">
								Более{' '}
								<strong className="aboutRow__selectText">
									150
								</strong>
								&nbsp;проектов
							</h3>
						</div>
					</div>
				</div>
			</section>
		)
	}
}
