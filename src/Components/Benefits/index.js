import React, { Component } from 'react'
import './style.css'
import { AttachMoney, Update, ThumbUp, VerifiedUser } from '@material-ui/icons/'

export default class Benefits extends Component {
	render() {
		return (
			<section className="section benefits" id="benefits">
				<div className="container">
					<div className="sectionHeader">
						<h2 className="sectionHeader__text">
							Наши преимущества
						</h2>
						<div className="sectionHeader__underline" />
					</div>
					<div className="cards">
						<div className="cards__block">
							<div className="cards__item">
								<div className="cards__icon">
									<AttachMoney className="cards__Micon"/>
								</div>
								<div className="cards__name">Стоимость</div>
								<div className="cards__desc">
									Работая напрямую с нами без посредников вы
									можете съэкономить до 20% от суммы всех
									работ.
								</div>
							</div>
							<div className="cards__item">
								<div className="cards__icon">
									<Update className="cards__Micon"/>
								</div>
								<div className="cards__name">
									Быстрые&nbsp;сроки
								</div>
								<div className="cards__desc">
									Начиная производство работ мы не берёмся за
									другие заказы, сосредотачиваясь на вашем
									проекте.
								</div>
							</div>
						</div>
						<div className="cards__block">
							<div className="cards__item">
								<div className="cards__icon">
									<ThumbUp className="cards__Micon"/>
								</div>
								<div className="cards__name">Качество</div>
								<div className="cards__desc">
									Имя огромный опыт и собственное производство
									мы используем только качественные материалы
									и строго следуем технологии.
								</div>
							</div>
							<div className="cards__item">
								<div className="cards__icon">
									<VerifiedUser className="cards__Micon"/>
								</div>
								<div className="cards__name">Гарантия</div>
								<div className="cards__desc">
									Мы уверены в своих изделиях, поэтому
									исправление любых ошибок за наш счёт.
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		)
	}
}
