import React, { Component } from 'react'
import './style.css'

export default class Reviews extends Component {
	render() {
		return (
			<section className="section reviews" id="reviews">
				<div className="container">
					<div className="sectionHeader">
						<h2 className="sectionHeader__text">
							Отзывы наших&nbsp;клиентов
						</h2>
						<div className="sectionHeader__underline" />
					</div>

					<div className="reviewsCarousel">
						<i className="material-icons">keyboard_arrow_left</i>
						<div className="reviewsCarousel__item reviewCard">
							<div className="reviewCard__imgwr">
								<img
									src="https://picsum.photos/150/150/?random"
									alt="Фото объекта"
									className="reviewCard__img"
								/>
							</div>
							<div className="reviewCard__name">Иванов В.В.</div>
							<div className="reviewCard__text">
								Lorem ipsum dolor sit amet consectetur,
								adipisicing elit. Molestiae ad esse, ducimus in
								possimus cumque!
							</div>
							<button className="reviewCard__btn">
								Видео отзыв
							</button>
						</div>
						<div className="reviewsCarousel__item reviewCard">
							<div className="reviewCard__imgwr">
								<img
									src="https://picsum.photos/150/150/?random"
									alt="Фото объекта"
									className="reviewCard__img"
								/>
							</div>
							<div className="reviewCard__name">Иванов В.В.</div>
							<div className="reviewCard__text">
								Lorem ipsum dolor sit amet consectetur
								adipisicing elit. Autem rem minus et quibusdam.
								Perspiciatis nemo illum ipsum at esse
								reiciendis!
							</div>
							<button className="reviewCard__btn">
								Видео отзыв
							</button>
						</div>
						<div className="reviewsCarousel__item reviewCard">
							<div className="reviewCard__imgwr">
								<img
									src="https://picsum.photos/150/150/?random"
									alt="Фото объекта"
									className="reviewCard__img"
								/>
							</div>
							<div className="reviewCard__name">Иванов В.В.</div>
							<div className="reviewCard__text">
								Lorem ipsum dolor sit amet consectetur
								adipisicing elit. Quia earum numquam corrupti
								accusantium accusamus ea praesentium modi velit,
								voluptas hic tempora quos mollitia natus maxime
								rerum veritatis pariatur quidem reprehenderit?
							</div>
							<button className="reviewCard__btn">
								Видео отзыв
							</button>
						</div>
						<i className="material-icons">keyboard_arrow_right</i>
					</div>
				</div>
			</section>
		)
	}
}
