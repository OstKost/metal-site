import React, { Component } from 'react'
import './styles/fonts.css'
import './App.css'

import CssBaseline from '@material-ui/core/CssBaseline'

import Header from './Components/Header'
import About from './Components/About'
import Projects from './Components/Projects'
import Benefits from './Components/Benefits'
import Stages from './Components/Stages'
import Offer from './Components/Offer'
import Reviews from './Components/Reviews'
import Footer from './Components/Footer'
import Copyright from './Components/Copyright'

class App extends Component {
	render() {
		return (
			<div className="App">
				<CssBaseline />
				<Header />
				<About />
				<Projects />
				<Benefits />
				<Stages />
				<Offer />
				<Reviews />
				<Footer />
				<Copyright />
			</div>
		)
	}
}

export default App
